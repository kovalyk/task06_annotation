package com.epam.controller;

import com.epam.model.CallingClass;
import com.epam.model.MyGenericClass;
import com.epam.model.MethodInvoker;
import com.epam.view.View;

import java.io.IOException;

public class Controller {
    private final static String DIVIDER = "----------------------------------------------------------------------" +
            "-----------------------------------------------------------------";
    private View view;
    private MethodInvoker methodInvoker;
    private CallingClass callingClass;

    public Controller(View view, MethodInvoker methodInvoker, CallingClass callingClass) throws IOException {
        this.view = view;
        view.controller = this;
        this.methodInvoker = methodInvoker;
        this.callingClass = callingClass;
        methodInvoker.view = view;
        callingClass.view = view;
        callingClass.methodInvoker = methodInvoker;
        callingClass.execute();
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void printFieldWithMyAnnotation() {
        view.logger.warn(DIVIDER);
        callingClass.printFieldWithMyAnnotation();
    }

    public void printValuesOfMyAnnotation() {
        view.logger.warn(DIVIDER);
        callingClass.printValuesOfMyAnnotation();
    }

    public void invokeThreeMethods() {
        view.logger.warn(DIVIDER);
        callingClass.invokeThreeMethods();
    }

    public void setValueWithUnknownType() {
        view.logger.warn(DIVIDER);
        callingClass.setValueWithUnknownType();
    }

    public void invokeMethodsWithStringArgs() {
        view.logger.warn(DIVIDER);
        callingClass.invokeMethodsWithStringArgs();
    }

    public void showInformationAboutClass() {
        view.logger.warn(DIVIDER);
        MyGenericClass myGenericClass = new MyGenericClass(MethodInvoker.class, view);
    }
}

