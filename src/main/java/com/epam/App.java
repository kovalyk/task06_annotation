package com.epam;

import com.epam.controller.Controller;
import com.epam.model.*;
import com.epam.view.View;

import java.io.IOException;

public class App {
    public static void main(String[] args) {
        try {
            new Controller(new View(), new MethodInvoker(), new CallingClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
