package com.epam.model;

import com.epam.view.View;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyGenericClass<T> {
    private final Class<T> myClass;
    View view;
    private T obj;

    public MyGenericClass(Class<T> myClass, View view) {
        this.myClass = myClass;
        try {
            obj = myClass.getConstructor().newInstance();

            view.logger.warn("The declared fields of class " + myClass.getSimpleName() + " are : ");
            Field[] fields = myClass.getDeclaredFields();
            for (Field field : fields) {
                view.logger.info("  " + field.getName() + " (" + field.getType() + ")");
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}


