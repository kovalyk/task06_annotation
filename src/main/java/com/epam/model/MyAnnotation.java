package com.epam.model;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
public @interface MyAnnotation {
    int age() default 18;
    String name() default "unknown";
}
