package com.epam.model;

import com.epam.view.View;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CallingClass {
    public View view;
    public MethodInvoker methodInvoker;
    Class myClass;

    public void execute() {
        myClass = methodInvoker.getClass();
    }

    public static void main(String[] args) {
        MethodInvoker methodInvoker = new MethodInvoker();
        CallingClass callingClass = new CallingClass();
        callingClass.invokeMethodsWithStringArgs();
    }

    public void printFieldWithMyAnnotation() {
        Field[] fields = myClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                view.logger.info(String.valueOf(field));
            }
        }
    }

    public void printValuesOfMyAnnotation() {
        Field[] fields = myClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                view.logger.info(String.valueOf(field.getDeclaredAnnotation(MyAnnotation.class)));
            }
        }
    }

    public void invokeThreeMethods() {
        Method[] methods = myClass.getDeclaredMethods();
        for (Method method : methods) {
            view.logger.info(String.valueOf(method));
        }
        try {//first method
            Method method = myClass.getDeclaredMethod("method1");
            method.invoke(methodInvoker);
            // second method
            Class[] paramTypes2 = new Class[]{String.class};
            Method method2 = myClass.getDeclaredMethod("method2", paramTypes2);
            method2.setAccessible(true);
            Object[] args = new Object[]{"my text"};
            method2.invoke(methodInvoker, args);
            //third method
            Method method3 = myClass.getDeclaredMethod("method3", new Class[]{int.class, int.class, String.class});
            method3.setAccessible(true);
            view.logger.info(String.valueOf(method3.invoke(methodInvoker, 2, 3, "Met.3 Product = ")));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void invokeMethodsWithStringArgs() {
        try {//String...
            view.logger.warn(" String[].class");
            Class[] argTypes1 = new Class[] { String[].class };
            Method myMethodStrings = myClass.getDeclaredMethod("myMethod", argTypes1);
            myMethodStrings.setAccessible(true);
            String[] mainArgs1 = new String[]{"Hello","world","!"};
            myMethodStrings.invoke(methodInvoker, (Object)mainArgs1);
            //String, int ...
            view.logger.warn(" String.class, int[].class ");
            Class[] argTypes2 = new Class[] {String.class, int[].class };
            Method myMethodStringInts = myClass.getDeclaredMethod("myMethod", argTypes2);
            myMethodStringInts.setAccessible(true);
            int[] mainArgs = new int[]{1, 2, 3, 4, 5};
            myMethodStringInts.invoke(methodInvoker,"Sum of numbers: ", (Object)mainArgs);

        } catch (NoSuchMethodException x) {
            x.printStackTrace();
        } catch (IllegalAccessException x) {
            x.printStackTrace();
        } catch (InvocationTargetException x) {
            x.printStackTrace();
        }
    }

    public void setValueWithUnknownType() {
        try {
            Field field = myClass.getDeclaredField("value3");
            field.setAccessible(true);
            view.logger.info( "Type: " + field.getType() + ", value = " + field.get(methodInvoker));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            Field field = myClass.getDeclaredField("value3");
            field.setAccessible(true);
            field.setInt(methodInvoker, 26);
            view.logger.info("New value = "+ field.get(methodInvoker));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
